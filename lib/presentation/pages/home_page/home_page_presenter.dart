import 'package:confiaradio/domain/use_cases/go_to_url_use_case_interface.dart';
import 'package:confiaradio/presentation/pages/home_page/bloc/home_page_cubit.dart';
import 'package:confiaradio/presentation/pages/home_page/bloc/home_page_state.dart';
import 'package:confiaradio/presentation/pages/home_page/home_page_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'home_page.dart';

class HomePagePresenter extends StatelessWidget {
  final HomePageCubit _cubit = HomePageCubit(
      goToUrlUseCase: GetIt.I<GoToUrlUseCaseInterface>(),
      initialState: HomePageState());

  static const String facebook = 'https://www.facebook.com/santosprodutions/';

  HomePagePresenter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (context) => _cubit,
        child: HomePage(
          model:
              HomePageModel(onFacebookTap: () => _cubit.goToFacebook(facebook)),
        ),
      );
}
