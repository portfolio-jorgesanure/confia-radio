import 'package:audio_service/audio_service.dart';
import 'package:confiaradio/presentation/app_strings.dart';
import 'package:confiaradio/presentation/app_theme.dart';
import 'package:confiaradio/presentation/pages/home_page/home_page_presenter.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:screen_size_test/screen_size_test.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: AppStrings.confiaRadio,
        theme: AppTheme.build(context),
        debugShowCheckedModeBanner: false,
        builder: (_, child) {
          final _child = child ?? SizedBox.shrink();

          return SafeArea(
              child:
                  // kDebugMode
                  false
                      //
                      ? ScreenSizeTest(child: _child)
                      : _child);
        },
        home: AudioServiceWidget(child: HomePagePresenter()),
      );
}
