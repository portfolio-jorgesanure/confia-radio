import 'package:audio_service/audio_service.dart';
import 'package:confiaradio/services/audio_player_task.dart';
import 'package:flutter/material.dart';
import 'package:confiaradio/presentation/components/audio_player/presentation/audio_player.dart'
    as ui;

class AudioPlayerPresenter extends StatefulWidget {
  const AudioPlayerPresenter({Key? key}) : super(key: key);

  @override
  _AudioPlayerPresenterState createState() => _AudioPlayerPresenterState();
}

class _AudioPlayerPresenterState extends State<AudioPlayerPresenter> {
  bool isConnecting = false;

  @override
  Widget build(BuildContext context) => StreamBuilder<bool>(
        stream: AudioService.runningStream,
        builder: (context, snapshot) {
          final running = snapshot.data ?? false;
          final isLoading = snapshot.connectionState != ConnectionState.active;

          return StreamBuilder<bool>(
            stream: AudioService.playbackStateStream
                .map((state) => state.playing)
                .distinct(),
            builder: (context, snapshot) {
              final playing = snapshot.data ?? false;
              final _isLoading = isLoading ||
                  snapshot.connectionState == ConnectionState.waiting ||
                  (!playing && isConnecting);

              if (playing) isConnecting = false;

              return ui.AudioPlayer(
                model: ui.AudioPlayerModel(
                  isLoading: _isLoading,
                  isPlaying: playing,
                  onPlayTap: () {
                    if (running) {
                      if (playing)
                        AudioService.pause();
                      else
                        AudioService.play();
                    } else {
                      isConnecting = true;
                      initPlayer();
                    }
                  },
                ),
              );
            },
          );
        },
      );

  void initPlayer() => AudioService.start(
        backgroundTaskEntrypoint: audioPlayerTaskEntrypoint,
        androidNotificationChannelName: 'Confía Radio',
        // Enable this if you want the Android service to exit the foreground state on pause.
        //androidStopForegroundOnPause: true,
        // androidNotificationColor: 0xFF2196f3,
        androidNotificationIcon: 'mipmap/ic_launcher',
        androidEnableQueue: true,
      );
}
