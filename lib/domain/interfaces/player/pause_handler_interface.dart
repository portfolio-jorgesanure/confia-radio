abstract class PauseHandlerInterface {
  Future<void> invoke();
}
