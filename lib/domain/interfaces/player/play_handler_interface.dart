abstract class PlayHandlerInterface {
  Future<void> invoke();
}
