import 'package:confiaradio/domain/interfaces/player/pause_handler_interface.dart';
import 'package:confiaradio/domain/use_cases/player/pause_use_case_interface.dart';

class PauseUseCase implements PauseUseCaseInterface {
  late PauseHandlerInterface _pauseHandler;

  PauseUseCase({required PauseHandlerInterface pauseHandler}) {
    _pauseHandler = pauseHandler;
  }

  @override
  Future<void> invoke() => _pauseHandler.invoke();
}
