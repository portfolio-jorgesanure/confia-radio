import 'package:confiaradio/domain/interfaces/player/play_handler_interface.dart';
import 'package:confiaradio/domain/use_cases/player/play_use_case_interface.dart';

class PlayUseCase implements PlayUseCaseInterface {
  late PlayHandlerInterface _playHandler;

  PlayUseCase({required PlayHandlerInterface playHandler}) {
    _playHandler = playHandler;
  }

  @override
  Future<void> invoke() => _playHandler.invoke();
}
