import 'package:confiaradio/domain/use_cases/go_to_url_use_case.dart';
import 'package:confiaradio/domain/use_cases/go_to_url_use_case_interface.dart';
import 'package:confiaradio/domain/use_cases/player/pause_use_case.dart';
import 'package:confiaradio/domain/use_cases/player/pause_use_case_interface.dart';
import 'package:confiaradio/domain/use_cases/player/play_use_case.dart';
import 'package:confiaradio/domain/use_cases/player/play_use_case_interface.dart';
import 'package:get_it/get_it.dart';

class AppSetter {
  AppSetter._();
  static Future<void> setUp() async {
    GetIt.I.registerSingleton<GoToUrlUseCaseInterface>(GoToUrlUseCase());
  }
}
