abstract class PlayUseCaseInterface {
  Future<void> invoke();
}
