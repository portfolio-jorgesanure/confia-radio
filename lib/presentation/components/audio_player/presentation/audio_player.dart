import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class AudioPlayerModel {
  final bool isPlaying;
  final bool isLoading;
  final Function onPlayTap;

  AudioPlayerModel(
      {required this.isLoading,
      required this.onPlayTap,
      required this.isPlaying});
}

class AudioPlayer extends StatelessWidget {
  final AudioPlayerModel model;

  const AudioPlayer({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) => Stack(
        alignment: Alignment.center,
        children: [
          Visibility(
              visible: model.isLoading,
              child: _buildLoadingShimmer(
                  child: Container(
                padding: const EdgeInsets.all(46),
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.white),
              ))),
          _buildAudioPlayer(context)
        ],
      );

  Widget _buildAudioPlayer(BuildContext context) => InkWell(
        focusColor: Colors.transparent,
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        onTap: () => model.onPlayTap(),
        child: Container(
          padding: const EdgeInsets.all(25),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                  color: Colors.black12, offset: Offset(1, 1), blurRadius: 2)
            ],
            border: Border.all(color: Colors.white70, width: 5),
            color: const Color(0xFFF1F3F4),
          ),
          child: Opacity(
            opacity: model.isLoading ? 0.2 : 1,
            child: Icon(
              model.isPlaying ? Icons.pause : Icons.play_arrow,
              size: 29,
            ),
          ),
        ),
      );

  Widget _buildLoadingShimmer({required Widget child}) => Shimmer.fromColors(
      baseColor: Colors.black12, highlightColor: Colors.white, child: child);
}
