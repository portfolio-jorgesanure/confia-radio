import 'package:confiaradio/domain/use_cases/go_to_url_use_case_interface.dart';
import 'package:confiaradio/presentation/pages/home_page/bloc/home_page_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePageCubit extends Cubit<HomePageState> {
  late GoToUrlUseCaseInterface _goToUrlUseCase;

  HomePageCubit(
      {required GoToUrlUseCaseInterface goToUrlUseCase,
      required HomePageState initialState})
      : super(initialState) {
    _goToUrlUseCase = goToUrlUseCase;
  }

  void goToFacebook(String url) => _goToUrlUseCase.invoke(url: url);
}
