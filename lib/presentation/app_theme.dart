import 'package:flutter/material.dart';

import 'app_fonts.dart';

class AppTheme {
  AppTheme._();

  static ThemeData build(BuildContext context) =>
      ThemeData(fontFamily: AppFonts.Jost);
}
