abstract class PauseUseCaseInterface {
  Future<void> invoke();
}
