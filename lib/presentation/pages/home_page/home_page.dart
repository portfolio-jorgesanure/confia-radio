import 'package:confiaradio/presentation/app_colors.dart';
import 'package:confiaradio/presentation/app_images.dart';
import 'package:confiaradio/presentation/app_strings.dart';
import 'package:confiaradio/presentation/components/audio_player/presentation/audio_player_presenter.dart';
import 'package:confiaradio/presentation/pages/home_page/home_page_model.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class HomePage extends StatelessWidget {
  final HomePageModel model;

  const HomePage({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(
          children: [
            // header
            _buildHeader(context),
            // content
            Expanded(
              child: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // player
                        AudioPlayerPresenter(),
                        // title
                        _buildTitle(),
                        // message
                        SizedBox(
                          child: Text(
                            'Una radio emisora para llevar aliento a personas como tu, que durante y después de la pandemia por el Coronavirus (Covid-19) necesitan fortaleza.',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 19),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );

  Widget _buildHeader(BuildContext context) => Container(
        color: Colors.white,
        padding: const EdgeInsets.symmetric(vertical: 8).copyWith(
            left: getValueForScreenType<double>(
                context: context, tablet: 40, mobile: 20),
            right: 20),
        child: Row(
          children: [
            SizedBox(
              height: 60,
              width: 60,
              child: Image.asset(AppImages.logo),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                AppStrings.confiaRadio,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
              ),
            ),
            InkWell(
              onTap: () => model.onFacebookTap(),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'f',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
                ),
              ),
            )
          ],
        ),
      );

  Widget _buildTitle() {
    final _style = TextStyle(
        fontSize: 78,
        fontWeight: FontWeight.bold,
        color: AppColors.FFE43F3F_red);

    final Widget Function(String) _textBuilder = (text) => FittedBox(
          fit: BoxFit.contain,
          child: LayoutBuilder(
            builder: (context, constraints) => Text(
              text.toUpperCase(),
              maxLines: 1,
              textAlign: TextAlign.center,
              style: _style,
            ),
          ),
        );

    return Column(
      children: [
        _textBuilder(AppStrings.confia.toUpperCase() + ' '),
        _textBuilder(' ' + AppStrings.radio.toUpperCase() + '  '),
      ],
    );
  }
}
