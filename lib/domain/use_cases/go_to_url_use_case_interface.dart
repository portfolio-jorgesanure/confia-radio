abstract class GoToUrlUseCaseInterface {
  Future<void> invoke({required String url});
}
