import 'package:flutter/material.dart';
import 'app_setter.dart';
import 'my_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await AppSetter.setUp();

  runApp(MyApp());
}
