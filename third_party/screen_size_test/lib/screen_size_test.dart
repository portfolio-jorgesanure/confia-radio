import 'package:flutter/material.dart';

class ScreenSizeTest extends StatefulWidget {
  final Widget? child;

  const ScreenSizeTest({Key? key, this.child}) : super(key: key);

  @override
  _ScreenSizeTestState createState() => _ScreenSizeTestState();
}

class _ScreenSizeTestState extends State<ScreenSizeTest> {
  double? _screenHeight;
  double? _screenHeightMax;
  double? _screenWidth;
  double? _screenWidthMax;

  final double _sliderHeight = 50;

  @override
  Widget build(BuildContext context) => LayoutBuilder(
        builder: (context, constraints) {
          if (_screenWidth == null)
            _screenWidthMax = _screenWidth = constraints.maxWidth;
          if (_screenHeight == null)
            _screenHeightMax =
                _screenHeight = constraints.maxHeight - (_sliderHeight * 2);

          return Column(
            children: [
              Expanded(
                  child: Column(
                children: [
                  Container(
                    width: _screenWidth,
                    height: _screenHeight,
                    child: widget.child,
                  ),
                ],
              )),
              _buildSlider(
                  max: _screenWidthMax,
                  value: _screenWidth,
                  label: 'W : ',
                  onChange: (value) {
                    if (value != 0.0) setState(() => _screenWidth = value);
                  }),
              _buildSlider(
                  max: _screenHeightMax,
                  label: 'H : ',
                  value: _screenHeight,
                  onChange: (value) {
                    if (value != 0.0) setState(() => _screenHeight = value);
                  }),
            ],
          );
        },
      );

  Widget _buildSlider(
          {String? label,
          double? max,
          double? value,
          Function()? onTap,
          Function(double)? onChange}) =>
      Container(
        alignment: Alignment.center,
        height: _sliderHeight,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: double.infinity,
        color: Colors.white,
        child: Builder(
          builder: (context) => Scaffold(
            backgroundColor: Colors.white,
            body: InkWell(
              onTap: onTap,
              child: Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: Text(label! + value!.round().toStringAsFixed(2))),
                  Expanded(
                    flex: 8,
                    child: Slider(
                      max: max!,
                      min: 0,
                      value: value,
                      onChanged: onChange,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
