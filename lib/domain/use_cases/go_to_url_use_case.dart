import 'package:url_launcher/url_launcher.dart';

import 'go_to_url_use_case_interface.dart';

class GoToUrlUseCase implements GoToUrlUseCaseInterface {
  @override
  Future<void> invoke({required String url}) async => _launchURL(url);

  Future<void> _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}
